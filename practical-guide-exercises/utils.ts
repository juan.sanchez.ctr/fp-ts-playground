import * as E from 'fp-ts/lib/Either'
import { constVoid } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'

export * from './inquirer'

export const runIfCli = (module: NodeModule) => <A>(task: T.Task<A>) =>
  require.main === module ? task() : constVoid()

export function assertLeft<L, R>(e: E.Either<L, R>): asserts e is E.Left<L> {
  if (e._tag !== 'Left') {
    throw new Error(`Expected 'Either' instance to be Left, but got Right: ${e.right}`)
  }
}

export function assertRight<L, R>(e: E.Either<L, R>): asserts e is E.Right<R> {
  if (e._tag !== 'Right') {
    throw new Error(`Expected 'Either' instance to be Right, but got Left: ${e.left}`)
  }
}

/** Make a jest function mock with better type inference about the function signature */
export const jestFn = <F extends (...args: any) => any>(
  f?: F
): jest.Mock<ReturnType<F>, Parameters<F>> => jest.fn(f)
