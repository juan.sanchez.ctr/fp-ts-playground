import { getOptionNumberGreaterThan5 } from './04-option-from-predicate'
import * as O from 'fp-ts/Option'

describe('option-from-predicate', () => {
  it('handles number greater than 5', () => {
    expect(getOptionNumberGreaterThan5(10)).toStrictEqual(O.some(10))
  })

  it('handles number equal to 5', () => {
    expect(getOptionNumberGreaterThan5(5)).toStrictEqual(O.none)
  })

  it('handles number less than 5', () => {
    expect(getOptionNumberGreaterThan5(2)).toStrictEqual(O.none)
  })
})
