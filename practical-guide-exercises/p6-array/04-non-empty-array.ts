/**
 * Exercise
 * - Update validateAnyContainerWithMoreThan5Packages to return true if the Container array contains
 *   any Containers with packages greater than 5, or else false using NEA.fromArray
 */

import { pipe } from 'fp-ts/lib/function'
import * as A from 'fp-ts/Array'
import * as NEA from 'fp-ts/NonEmptyArray'
import * as O from 'fp-ts/Option'

export interface Container {
  id: string
  packages: number
}

export const validateAnyContainerWithMoreThan5Packages = (containers: Container[]): boolean =>
  pipe(
    containers
    // TODO Implementation here
  )
