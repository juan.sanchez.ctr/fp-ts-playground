import {
  findProductOfColor,
  findProductOfColorWithDefault,
  Product,
  DefaultProduct,
} from './02-find-first'
import * as O from 'fp-ts/Option'

describe('find-first', () => {
  const product1: Product = {
    id: '1',
    color: 'RED',
  }
  const product2: Product = {
    id: '2',
    color: 'BLUE',
  }
  const product3: Product = {
    id: '3',
    color: 'GREEN',
  }
  const product4: Product = {
    id: '4',
    color: 'RED',
  }
  const product5: Product = {
    id: '5',
    color: 'BLUE',
  }
  const product6: Product = {
    id: '6',
    color: 'GREEN',
  }
  const products = [product1, product2, product3, product4, product5, product6]

  describe('findProductOfColor', () => {
    it.each`
      color           | expectedOutput
      ${'RED'}        | ${O.some(product1)}
      ${'BLUE'}       | ${O.some(product2)}
      ${'GREEN'}      | ${O.some(product3)}
      ${'CHARTREUSE'} | ${O.none}
    `(`handles finding product of color $color`, ({ color, expectedOutput }) => {
      expect(findProductOfColor(products, color)).toStrictEqual(expectedOutput)
    })
  })

  describe('findProductOfColorWithDefault', () => {
    it.each`
      color           | expectedOutput
      ${'RED'}        | ${product1}
      ${'BLUE'}       | ${product2}
      ${'GREEN'}      | ${product3}
      ${'CHARTREUSE'} | ${DefaultProduct}
    `(`handles finding product of color $color`, ({ color, expectedOutput }) => {
      expect(findProductOfColorWithDefault(products, color)).toStrictEqual(expectedOutput)
    })
  })
})
