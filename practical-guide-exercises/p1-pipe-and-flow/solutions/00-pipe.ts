/**
 * Welcome!  These exercises follow the Practical Guide to fp-ts.
 * Be sure to read part 1 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-1
 *
 * Exercise:
 *  - Modify helloWithGreeting to use pipe
 */

import { pipe } from 'fp-ts/lib/function'

const hello = (name: string) => `Hello ${name}!`

const appendSpace = (str: string) => str + ' '

const appendGreeting = (str: string) => `${str}Nice to meet you!`

export type GreetingFunction = (name: string) => string

// TODO modify this function to use pipe
export const helloWithGreeting: GreetingFunction = (name: string) =>
  pipe(hello(name), appendSpace, appendGreeting)
