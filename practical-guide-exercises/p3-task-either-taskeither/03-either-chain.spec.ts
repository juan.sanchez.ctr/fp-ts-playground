import * as E from 'fp-ts/lib/Either'
import { assertLeft, assertRight } from '../utils'
import { multiplyPositiveFractionNumberBy5 } from './03-either-chain'

describe('either', () => {
  it('multiplyPositiveFractionNumberBy5 returns an either left when the input is not a fraction', () => {
    const result = multiplyPositiveFractionNumberBy5(10)
    assertLeft(result)
    expect(result.left.type).toStrictEqual('FRACTION_NUMBER_ERROR')
  })
  it('multiplyPositiveFractionNumberBy5 returns an either left when the input is negative', () => {
    const result = multiplyPositiveFractionNumberBy5(-0.12)
    assertLeft(result)
    expect(result.left.type).toStrictEqual('POSITIVE_NUMBER_ERROR')
  })
  it('multiplyPositiveFractionNumberBy5 returns an either right with the proper calculation', () => {
    const result = multiplyPositiveFractionNumberBy5(0.12)
    assertRight(result)
    expect(result).toStrictEqual(E.right(0.6))
  })
})
