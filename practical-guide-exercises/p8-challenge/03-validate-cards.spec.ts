import * as E from 'fp-ts/Either'
import { Card } from './01-types'
import { validateCards } from './03-validate-cards'

describe('validate-cards', () => {
  const card1: Card = {
    id: '1',
    status: 'ACTIVE',
    network: 'VISA',
  }
  const card2: Card = {
    id: '2',
    status: 'INACTIVE',
    network: 'VISA',
  }
  const card3: Card = {
    id: '3',
    status: 'ACTIVE',
    network: 'MASTERCARD',
  }
  const card4: Card = {
    id: '4',
    status: 'INACTIVE',
    network: 'MASTERCARD',
  }
  const card5: Card = {
    id: '5',
    status: 'ACTIVE',
  }
  const card6: Card = {
    id: '6',
    status: 'INACTIVE',
  }
  it('should handle receiving an empty card array and return NoValidCardsFoundError', () => {
    const cards = []
    const res = validateCards({})(cards)

    expect(E.isLeft(res) && res.left.type === 'NO_VALID_CARDS_FOUND').toBe(true)
  })

  it('should handle all cards validated by networkType and return NoValidCardsFoundError', () => {
    const cards = [card1, card2]
    const res = validateCards({ networkType: 'MASTERCARD' })(cards)

    expect(E.isLeft(res) && res.left.type === 'NO_VALID_CARDS_FOUND').toBe(true)
  })

  it('should handle all cards validated by cardStatus and return NoValidCardsFoundError', () => {
    const cards = [card2, card4]
    const res = validateCards({ cardStatus: 'ACTIVE' })(cards)

    expect(E.isLeft(res) && res.left.type === 'NO_VALID_CARDS_FOUND').toBe(true)
  })

  it('should handle all cards validated by networkType and cardStatus and return NoValidCardsFoundError', () => {
    const cards = [card1, card2, card4, card5, card6]

    const res = validateCards({ cardStatus: 'ACTIVE', networkType: 'MASTERCARD' })(cards)

    expect(E.isLeft(res) && res.left.type === 'NO_VALID_CARDS_FOUND').toBe(true)
  })

  it.each`
    testNum | networkType     | cardStatus    | expectedOutput
    ${1}    | ${'VISA'}       | ${undefined}  | ${[card1, card2]}
    ${2}    | ${'MASTERCARD'} | ${undefined}  | ${[card3, card4]}
    ${3}    | ${undefined}    | ${'ACTIVE'}   | ${[card1, card3, card5]}
    ${4}    | ${undefined}    | ${'INACTIVE'} | ${[card2, card4, card6]}
    ${5}    | ${'VISA'}       | ${'ACTIVE'}   | ${[card1]}
    ${6}    | ${'VISA'}       | ${'INACTIVE'} | ${[card2]}
    ${7}    | ${'MASTERCARD'} | ${'ACTIVE'}   | ${[card3]}
    ${8}    | ${'MASTERCARD'} | ${'INACTIVE'} | ${[card4]}
  `(`should handle validating cards - $testNum`, ({ networkType, cardStatus, expectedOutput }) => {
    const cards = [card1, card2, card3, card4, card5, card6]
    const res = validateCards({ networkType, cardStatus })(cards)
    expect(res).toEqual(E.right(expectedOutput))
  })
})
