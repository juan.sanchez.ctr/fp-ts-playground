/**
 * Exercise
 * - Update validateCards to only return Cards that match the expected networkType and cardStatus if provided
 * - If networkType is provided, all Cards returned must match the networkType
 * - If cardStatus is provided, all Cards returned must match the cardStatus
 * - If networkType and cardStatus are not provided, return the original Card array
 * - If the Card array to be returned is empty, return E.left(NoValidCardsFoundError)
 */

import { Card } from './01-types'
import { CardNetwork, CardStatus } from './card-api'
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Array'
import * as NEA from 'fp-ts/NonEmptyArray'
import { NoValidCardsFoundError } from './errors'
import { pipe } from 'fp-ts/lib/function'

export const validateCards = ({
  networkType,
  cardStatus,
}: {
  networkType?: CardNetwork
  cardStatus?: CardStatus
}) => (cards: Card[]): E.Either<NoValidCardsFoundError, Card[]> =>
  pipe(
    cards
    // TODO Implementation here
  )
