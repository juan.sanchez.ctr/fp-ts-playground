import { UserEvent } from './02-union'
import * as E from 'fp-ts/Either'

describe('union', () => {
  it('handles an invalid input', () => {
    const input = {
      such: 'a',
      fake: 'event',
    }
    expect(UserEvent.is(input)).toBe(false)
  })

  it('handles a USER_CREATED event', () => {
    const input = {
      user_id: '1',
      event_name: 'USER_CREATED' as const,
      create_date: '2022-09-01',
    }
    expect(UserEvent.is(input)).toBe(true)
    expect(UserEvent.decode(input)).toStrictEqual(
      E.right({
        user_id: '1',
        event_name: 'USER_CREATED' as const,
        create_date: '2022-09-01',
      })
    )
  })

  it('handles a USER_NAME_UPDATED event - 1', () => {
    const input = {
      user_id: '1',
      event_name: 'USER_NAME_UPDATED' as const,
      first_name: 'Malcolm',
      last_name: 'McCormick',
    }
    expect(UserEvent.is(input)).toBe(true)
    expect(UserEvent.decode(input)).toStrictEqual(
      E.right({
        user_id: '1',
        event_name: 'USER_NAME_UPDATED' as const,
        first_name: 'Malcolm',
        last_name: 'McCormick',
      })
    )
  })

  it('handles a USER_NAME_UPDATED event - 2', () => {
    const input = {
      user_id: '1',
      event_name: 'USER_NAME_UPDATED' as const,
      first_name: 'Malcolm',
    }
    expect(UserEvent.is(input)).toBe(true)
    expect(UserEvent.decode(input)).toStrictEqual(
      E.right({
        user_id: '1',
        event_name: 'USER_NAME_UPDATED' as const,
        first_name: 'Malcolm',
      })
    )
  })

  it('handles a USER_ADDRESS_UPDATED event - 1', () => {
    const input = {
      user_id: '1',
      event_name: 'USER_ADDRESS_UPDATED' as const,
      address: {
        address_1: '2407 J St',
        city: 'Sacramento',
        state: 'CA',
        zip: '95816',
        country: 'USA',
      },
    }
    expect(UserEvent.is(input)).toBe(true)
    expect(UserEvent.decode(input)).toStrictEqual(
      E.right({
        user_id: '1',
        event_name: 'USER_ADDRESS_UPDATED' as const,
        address: {
          address_1: '2407 J St',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
      })
    )
  })

  it('handles a USER_ADDRESS_UPDATED event - 2', () => {
    const input = {
      user_id: '1',
      event_name: 'USER_ADDRESS_UPDATED' as const,
      address: {
        address_1: '2407 J St',
        address_2: 'Suite 300',
        city: 'Sacramento',
        state: 'CA',
        zip: '95816',
        country: 'USA',
      },
      mailing_address: {
        address_1: '53 Beach St',
        address_2: '2nd Floor',
        city: 'New York',
        state: 'NY',
        zip: '10013',
        country: 'USA',
      },
    }
    expect(UserEvent.is(input)).toBe(true)
    expect(UserEvent.decode(input)).toStrictEqual(
      E.right({
        user_id: '1',
        event_name: 'USER_ADDRESS_UPDATED' as const,
        address: {
          address_1: '2407 J St',
          address_2: 'Suite 300',
          city: 'Sacramento',
          state: 'CA',
          zip: '95816',
          country: 'USA',
        },
        mailing_address: {
          address_1: '53 Beach St',
          address_2: '2nd Floor',
          city: 'New York',
          state: 'NY',
          zip: '10013',
          country: 'USA',
        },
      })
    )
  })
})
